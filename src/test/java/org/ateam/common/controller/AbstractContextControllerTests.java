package org.ateam.common.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

@WebAppConfiguration
@ContextConfiguration({
	"file:src/main/webapp/WEB-INF/spring-security.xml",
	"file:src/main/webapp/WEB-INF/db-configuration.xml",
	"file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml",
})
public class AbstractContextControllerTests {

	@Autowired
	protected WebApplicationContext wac;

}