package org.ateam.common.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import javax.servlet.Filter;

import org.ateam.common.model.Token;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringJUnit4ClassRunner.class)
public class JSONControllerTest extends AbstractContextControllerTests{

	private MockMvc mockMvc;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	private Filter springSecurityFilterChain;

	
	@Before
	public void setup() {
		this.mockMvc = webAppContextSetup(wac)
                .addFilters(springSecurityFilterChain)
                .build();
	}
	
	@Test
	public void controllerExceptionHandler() throws Exception {
		MvcResult mvcResult = this.mockMvc.perform(get("/oauth/token?grant_type=password&client_id=rajith-client-id&client_secret=12345&username=rajith&password=password"))
				.andExpect(status().isOk())
				.andReturn();
		Token token = mapper.readValue(mvcResult.getResponse().getContentAsString(),Token.class);
		
		mvcResult = this.mockMvc.perform(get("/oauth/token?grant_type=refresh_token&client_id=rajith-client-id&refresh_token="+token.getRefreshToken()+"&client_secret=12345"))
				.andExpect(status().isOk())
				.andReturn();
		token = mapper.readValue(mvcResult.getResponse().getContentAsString(),Token.class);
		
		mvcResult = this.mockMvc.perform(get("/test/ateam?access_token="+token.getAccessToken()))
				.andExpect(status().isOk())
				.andReturn();
	}
}
